import {request} from "@/network/request";

export function getMeArticle(skip=0, limit=20) {
  return request({
    url: '/article/me_all',
    method: 'get',
    params: {
      skip,
      limit,
    },
  })
}
