import {request} from "@/network/request";


export function getUserInfo(){
    return request({
    url: '/user/me',
    method: 'get',
  })
}

export function getUser(){
  return request({
    url: '/user/details',
    method: 'get'
  })
}
