import axios from "axios"
import store from "../store/index";
import {tokenRefreshVerdict} from "./util";
import {baseURL} from "./url";
import {refreshToken} from "./util";
import router from "@/router";
import {DEL_TOKEN, SET_TOKEN} from "@/store/mutations-types";

const CancelToken = axios.CancelToken;
const source = CancelToken.source();

// 不需要token的路由
const notToken = ['/login/access-token']

export function request(config) {
  const instance = axios.create({
    baseURL,
    timeout: 5000,
    cancelToken: source.token
  })

  // token 无感刷新处理，根据后端返回的过期时间。在临近过期一天前及以后，开始刷新token。成功写入token, 失败则重新登录。
  // 拦截器，拦截请求
  instance.interceptors.request.use(
    async (config) => {
      if (notToken.some(url => url === config.url)) return config

      // 刷新token
      if (tokenRefreshVerdict(store.state.token)) {
        try {
          let newToken = await refreshToken(store.state.token)
          store.commit(SET_TOKEN, newToken.data)
        } catch (err) {
          console.log(err.response, '身份过期')
          store.commit(DEL_TOKEN)
          try {
            await router.push('/login')
          } catch {
          }
          return
        }
      }
      config.headers['Authorization'] = store.getters.getAuth
      return config
    }, (err) => {
    }
  )

  // 响应拦截
  instance.interceptors.response.use(
    (res) => {
      return res.data
    },
    (err) => {
      return Promise.reject(err)
    }
  )
  return instance(config)
}


