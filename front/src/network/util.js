import axios from "axios"
import {baseURL} from "@/network/url";

export async function refreshToken(token) {
  const tokenType = token['token_type'] + ' '
  return axios.put(baseURL + '/login/access-token',
    {},
    {
      headers: {
        'Authorization': tokenType + token['access_token'],
        'refresh-token': tokenType + token['refresh_token']
      }
    })
}


export function tokenRefreshVerdict(token) {
  // token 过期.
  let tokenDate = token.token_period_time * 1000

  let now = new Date()
  let utc = now.getTimezoneOffset() * 60 * 1000
  utc = utc + now.getTime()

  return tokenDate <= utc;
}

function timestampToTime(timestamp) {
  if ((timestamp + '').length === 10) timestamp *= 1000
  const date = new Date(timestamp);
  const Y = date.getFullYear() + '-';
  const M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
  const D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
  const h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
  const m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
  const s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
  return Y + M + D + h + m + s;
}
