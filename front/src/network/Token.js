import {request} from "@/network/request";
import qs from "qs";

/**
 * 业务逻辑：配置拦截器，请求发送前没有token 则跳转到登录页面。所有返回头检测有没有 字段，有则 刷新token
 * @param username
 * @param password
 * @returns {AxiosPromise}
 */

export function getToken(username, password) {

  return request({
    url: '/login/access-token',
    method: 'POST',
    data: qs.stringify({
      username,
      password,
    }),
    headers: {'content-type': 'application/x-www-form-urlencoded'}
  })
}

