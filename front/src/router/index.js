import Vue from "vue";
import VueRouter from "vue-router";

import store from "../store/index"


const Login = () => import("@/views/login/Login")

const Profile = () => import("@/views/profile/Profile")
const Welcome = () => import('@/views/profileChildren/welcome/Welcome')

const User = () => import("@/views/profileChildren/user/User")
const Test = () => import("@/views/profileChildren/user/children/Test")

const Article = () => import("@/views/profileChildren/article/Article")
const All = ()=> import("@/views/profileChildren/article/children/All")

Vue.use(VueRouter);

// user 子路由

const profile = {
  path: '/profile',
  component: Profile,
  children: [
    {path: '', component: Welcome},
    {path: 'user', component: User},
    {path: 'user/test', component: Test},
    {path: 'article', component: Article},
    {path: 'article/all', component: All},
  ]
}

const routes = [
  profile,
  {path: "/login", component: Login},
  // 404, 未定义
  {path: '*', redirect: '/profile'}
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

// 没有token 跳转到登录页面
router.beforeEach((to, from, next) => {
  const token = !!store.getters.getAuth
  if (to.path === '/login') next()
  else token ? next() : next('/login')
})

export default router;
