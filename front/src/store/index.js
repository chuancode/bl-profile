import Vue from 'vue'
import Vuex from 'vuex'

import {Storages} from "@/common/storage";
import {DEL_TOKEN, SET_TOKEN} from "@/store/mutations-types";

Vue.use(Vuex)


export default new Vuex.Store({
  state: {
    token: Storages.get('token')
  },
  mutations: {
    [SET_TOKEN](state, token) {
      state.token = token
      Storages.set('token', token)
    },
    [DEL_TOKEN](state) {
      state.token = null
      Storages.remove('token')
    }
  },
  getters: {
    getAuth(state) {
      const token = state.token
      if (token) {
        return token.token_type + ' ' + token.access_token
      } else return null
    },
  },
  actions: {},
  modules: {}
})
