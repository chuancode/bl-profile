// let store = window.localStorage

// 缓存至本地，两种方式可选
export class Storages {
  static Store = window.localStorage

  static config(type) {
    if (type === "local") {
      Storages.Store = window.localStorage
    } else if (type === "session") {
      Storages.Store = window.sessionStorage
    }
  }


  static get(key) {
    let value = Storages.Store.getItem(key)
    try {
      value = JSON.parse(value)
    } catch (err) {
      // console.error('Storage', err)
      value = null
    }
    return value
  }

  static set(key, val) {
    Storages.Store.setItem(key, JSON.stringify(val))
  }

  static remove(key) {
    Storages.Store.removeItem(key)
  }

  static clear() {
    Storages.Store.clear()
  }
}
