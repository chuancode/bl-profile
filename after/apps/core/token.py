from datetime import timedelta, datetime
from typing import Union, Any

from jose import jwt
from werkzeug.security import generate_password_hash, check_password_hash

from core.settings import setting

"""
flask 项目使用的密码解析是：werkzeug.security.generate_password_hash
此处为了兼容也是用它，而没有使用 passlib
"""

# jwt 算法
ALGORITHM = "HS256"


def create_token(subject: Union[str, Any], expires_delta: timedelta = None):
    """
    :param subject: 根据此参数构建 token的 主要识别项
    :param expires_delta: 超时时间
    :return: token
    """
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(seconds=setting.ACCESS_TOKEN_EXPIRE_TIME)

    to_encode = {"exp": expire, "subject": str(subject)}
    return jwt.encode(to_encode, setting.ACCESS_SECRET_KEY, algorithm=ALGORITHM)


def verify_password(plain_pw: str, hashed_pw: str) -> bool:
    return check_password_hash(hashed_pw, plain_pw)


def get_password_hash(pw: str) -> str:
    return generate_password_hash(password=pw)
