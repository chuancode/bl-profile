from datetime import datetime, timedelta

from pydantic import BaseSettings

__TAGS_METADATA__ = [
    {
        "name": "user",
        "description": "用户相关！"
    },
    {
        "name": "article",
        "description": "文章",
        "externalDocs": {
            "description": "更多文章请看",
            "url": "https://hcuan.top/",
        },
    }
]

__CORS_ORIGINS__ = {"http://localhost:8080",
                    "http://192.168.3.3:8081", "http://192.168.3.3:8080",
                    "http://local.dockertoolbox.tiangolo.com",
                    "https://hcuan.top"}

__DEFINED_STATUS__ = {
    "482": "refresh token 过期，或者其实体无效！",
}


class Settings(BaseSettings):
    """
    PROJECT:

    """
    PROJECT_NAME = "blog profile api"
    PROJECT_DESC = "单独抽离出来可能会麻烦，是为了练手"
    PROJECT_VERSION = "1.0.0"
    API_V1_STR: str = "/v1"
    TAGS_METADATA = __TAGS_METADATA__
    DEFINED_STATUS = __DEFINED_STATUS__

    STATIC_ICON_DIR = 'https://hcuan.top/static/'
    CORS_ORIGINS = __CORS_ORIGINS__

    # secode
    ACCESS_TOKEN_EXPIRE_TIME: int = 60 * 60 * 24 * 7
    # bug ,  此处实例化后。它的值就为一个固定值了。改为方法名动态获取此值
    ACCESS_TOKEN_REFRESH_NAME = "6c81816caa6cf63b88e8d09d25e06b7"
    ACCESS_TOKEN_REFRESH_TIME: int = ACCESS_TOKEN_EXPIRE_TIME * 2
    ACCESS_SECRET_KEY = "6c81816caa6cf63b88e8d09d25e06b7a9563b93f7099f6f0f494faa6ca2553e7"
    # # password密钥？来自 flask
    # SECRET_KEY = 'keystones234241join21aids'


class ProSettings(Settings):
    SQLALCHEMY_DATABASE_URI = ''


class DevSettings(Settings):
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://root:1qaz2w@192.168.3.33:3306/pro_blog_1_1"

    ME_UID = 1


setting = DevSettings()
