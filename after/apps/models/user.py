__all__ = ["UserToken", "UserImg"]

from sqlalchemy import Column, String, ForeignKey, Integer
from sqlalchemy.orm import relationship

from apps.db.base_class import Base


class UserToken(Base):
    __tablename__ = 'user_token'

    name = Column(String(16))
    email = Column(String(64), nullable=False, unique=True)
    token = Column(String(128), nullable=False)
    remark = Column(String(512))
    icon = Column(String(256))
    img_num = Column(Integer)

    articles = relationship('Article', back_populates="user")


class UserImg(Base):
    __tablename__ = 'user_img'

    file_name = Column(String(256), nullable=False)
    user_id = Column(ForeignKey('user_token.id'), nullable=False, index=True)

    user = relationship('UserToken')
