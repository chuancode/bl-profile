__all__ = ['ArticleType', "Tag", "Series", "Article", "ArticleTag", "Comment"]

from sqlalchemy import Column, ForeignKey, String, Integer, Text
from sqlalchemy.orm import relationship

from apps.db.base_class import Base


class ArticleType(Base):
    __tablename__ = 'article_type'

    name = Column(String(16), nullable=False)


class Tag(Base):
    __tablename__ = 'tag'

    name = Column(String(8), nullable=False, unique=True)


class Series(Base):
    __tablename__ = 'series'

    title = Column(String(128), nullable=False)
    description = Column(String(1024))
    uploader = Column(ForeignKey('user_token.id'), nullable=False, index=True)
    s_click = Column(Integer, nullable=False)
    type_id = Column(ForeignKey('article_type.id'), nullable=False, index=True)

    type = relationship('ArticleType')
    user_token = relationship('UserToken')


class Article(Base):
    __tablename__ = 'article'

    title = Column(String(128), nullable=False)
    content = Column(Text, nullable=False)
    username = Column(ForeignKey('user_token.id'), nullable=False, index=True)
    click = Column(Integer)
    a_type = Column(ForeignKey('article_type.id'), nullable=False, index=True)
    is_series = Column(ForeignKey('series.id'), index=True)

    article_type = relationship('ArticleType')
    series = relationship('Series')
    user = relationship('UserToken', back_populates="articles")
    tags = relationship('Tag', secondary="article_tag")


class ArticleTag(Base):
    __tablename__ = 'article_tag'

    article_id = Column(ForeignKey('article.id'), nullable=False, index=True)
    tag_id = Column(ForeignKey('tag.id'), nullable=False, index=True)

    article = relationship('Article')
    tag = relationship('Tag')


class Comment(Base):
    __tablename__ = 'comment'

    article_id = Column(ForeignKey('article.id'), nullable=False, index=True)
    content = Column(Text, nullable=False)
    name = Column(String(16), nullable=False)
    email = Column(String(64), nullable=False)
    father_id = Column(ForeignKey('comment.id'), index=True)
    icon = Column(String(256))
    reply_father_id = Column(ForeignKey('comment.id'), index=True)
    web_url = Column(String(64))

    article = relationship('Article')
    # father = relationship('Comment', remote_side=[id], primaryjoin='Comment.father_id == Comment.id')
    # reply_father = relationship('Comment', remote_side=[id], primaryjoin='Comment.reply_father_id == Comment.id')
