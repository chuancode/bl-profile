from typing import Optional

from pydantic import BaseModel, validator

from core.settings import setting


class UserBase(BaseModel):
    name: str = None
    # email 本身不允许为空，但是怕 检测报错
    icon: str = None


class UserInDB(UserBase):
    id: Optional[int]

    class Config:
        orm_mode = True


# 通过 api 返回，其中自定义字段
class User(UserInDB):

    @validator('icon')
    def check_icon(cls, v):
        return setting.STATIC_ICON_DIR + v


class UserDetail(User):
    email: str = None
    remark: str = None
