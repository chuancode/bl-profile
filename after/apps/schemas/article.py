import datetime
from typing import Optional, List

from pydantic import BaseModel, validator, Field
from pydantic.types import constr


class ArticleBase(BaseModel):
    title: str
    click: int = 0


class SeriesBase(BaseModel):
    id: int
    title: str = ''

    class Config:
        orm_mode = True


# -----------------------


class Tag(BaseModel):
    name: str

    class Config:
        orm_mode = True


class ArticleInDB(ArticleBase):
    id: int
    date: datetime.datetime = Field(None, alias="date_time")
    time: datetime.datetime = Field(None, alias="date_time")
    content: str

    class Config:
        orm_mode = True

    @validator('time')
    def check_time(cls, v: datetime.datetime):
        return v.timestamp()


# 显示简略信息, 不包括用户信息等
class Article(ArticleInDB):
    series: Optional[SeriesBase]
    tags: List[Tag] = Field([])


class ArticleLimit(BaseModel):
    num: int
    content: List[Article]


# 详细信息供显示
class ArticleDetail(Article):
    ...
