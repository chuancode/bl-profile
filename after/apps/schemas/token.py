from datetime import timedelta, datetime
from typing import Optional

from pydantic import BaseModel, validator


class Token(BaseModel):
    access_token: str
    refresh_token: str
    token_period_time: int
    token_type: str


class TokenData(BaseModel):
    subject: Optional[str] = None
    exp: datetime = None
