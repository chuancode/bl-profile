from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from api import api_v1
from core.settings import setting


def create_app() -> FastAPI:
    app = FastAPI(
        title=setting.PROJECT_NAME,
        description=setting.PROJECT_DESC,
        version=setting.PROJECT_VERSION,
        openapi_tags=setting.TAGS_METADATA
    )

    app.add_middleware(
        CORSMiddleware,
        allow_origins=setting.CORS_ORIGINS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    app.include_router(api_v1.api_router, prefix=setting.API_V1_STR)

    return app


app = create_app()

if __name__ == '__main__':
    import uvicorn
    uvicorn.run('main:app', host="192.168.3.3", port=5000, reload=True)
