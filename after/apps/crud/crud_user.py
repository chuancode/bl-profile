from typing import Optional

from sqlalchemy.orm import Session

from core.token import verify_password
from crud.base import CRUDBase
from models.user import UserToken


class CRUDUser(CRUDBase[UserToken]):

    def get_by_email(self, db: Session, *, email: str):
        return db.query(UserToken).filter(UserToken.email == email).first()

    def authenticate(self, db: Session, *, email: str, password: str) -> Optional[UserToken]:

        user = self.get_by_email(db, email=email)

        if not user:
            return None
        if not verify_password(plain_pw=password, hashed_pw=user.token):
            return None

        return user


user = CRUDUser(UserToken)
