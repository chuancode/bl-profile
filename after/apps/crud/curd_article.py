from typing import Any, List

from sqlalchemy.orm import Session

import models
from crud.base import CRUDBase
from models.article import Article
from models.user import UserToken


class CRUDArticle(CRUDBase[Article]):

    def get_user_all(self, db: Session, user: UserToken, *, skip: int = 0, limit: int = 10) \
            -> List[Article]:

        return db.query(self.model).filter(self.model== user.id).offset(skip).limit(limit).all()


article = CRUDArticle(Article)
