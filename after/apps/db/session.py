from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from core.settings import setting

engine = create_engine(setting.SQLALCHEMY_DATABASE_URI, pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
