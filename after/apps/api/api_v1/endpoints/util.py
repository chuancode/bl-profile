from datetime import datetime, timedelta

from core import token
from core.settings import setting


def build_token(project):
    # 构建令牌带刷新功能, 自动根据配置更新有效时间

    return {
        "refresh_token": token.create_token(setting.ACCESS_TOKEN_REFRESH_NAME,
                                            expires_delta=timedelta(seconds=setting.ACCESS_TOKEN_REFRESH_TIME)),
        "access_token": token.create_token(project),
        "token_period_time": (datetime.utcnow() + timedelta(seconds=setting.ACCESS_TOKEN_EXPIRE_TIME)).timestamp(),
        "token_type": "bearer",
    }
