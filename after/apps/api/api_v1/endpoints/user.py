from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

import crud
import models
import schemas
from api import deps

router = APIRouter()


@router.get('/me', response_model=schemas.User, description="获取当前用户信息")
async def get_me(user=Depends(deps.get_current_user)):
    """
    get current user
    """
    return user


@router.get('/details', response_model=schemas.UserDetail, description='当前用户详情用户')
async def user_details(user=Depends(deps.get_current_user)):
    """
        get current user detail
    """
    return user
