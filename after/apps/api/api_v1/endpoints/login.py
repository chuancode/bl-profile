from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session

import crud
import schemas
from api import deps
from api.api_v1.endpoints.util import build_token

router = APIRouter()

"""
业务逻辑：增加使用有效token换取新token的路由，返回新token
"""


@router.post("/access-token", response_model=schemas.Token,
             description="获取 token, username => email; 请设置在header字段 Authorization: Bearer token")
async def access_token(db: Session = Depends(deps.get_db),
                       form_data: OAuth2PasswordRequestForm = Depends()):
    # 使用 oauth2 令牌登录
    user = crud.user.authenticate(db, email=form_data.username, password=form_data.password)

    if not user:
        raise HTTPException(status_code=404, detail="no such user")
    return build_token(user.id)


@router.put("/access-token", response_model=schemas.Token, description="更新令牌，header需包含 refresh-token,Authorization")
async def access_token_refresh(user=Depends(deps.get_token_refresh)):
    # 使用解析出的user 构架新的token, 同时刷新 refresh-token。reresh过期返回482，
    return build_token(user.id)
