from typing import List

from fastapi import APIRouter, Depends

import schemas
from api import deps

router = APIRouter()


@router.get('/me_all', response_model=schemas.ArticleLimit, response_model_by_alias=False, description='当前用户下所有文章')
async def me_all(user=Depends(deps.get_current_user), skip: int = 0, limit: int = 10, ):
    data = {
        'num': len(user.articles),
        'content': user.articles[skip:limit]
    }
    return data
