from fastapi import APIRouter

from .endpoints import user, article, login

api_router = APIRouter()

api_router.include_router(user.router, prefix='/user', tags=['user'])
api_router.include_router(article.router, prefix='/article', tags=['article'])
api_router.include_router(login.router, prefix='/login', tags=['longin'])
