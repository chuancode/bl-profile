from datetime import datetime
from typing import Any

from fastapi import Depends, HTTPException, Header
from fastapi.security import OAuth2PasswordBearer
from fastapi.security.utils import get_authorization_scheme_param
from jose import jwt
from pydantic import ValidationError
from sqlalchemy.orm import Session

import crud
import models
import schemas
from core.settings import setting
from db.session import SessionLocal

oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{setting.API_V1_STR}/login/access-token"
)


def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


# 私有工具

def __util_oauth2(refresh_token: str = Header(..., description='Bearer token...')) -> str:
    """
    手动解析 refresh_token 字段中的 bearer令牌
    :param refresh_token: header 中的 jwt字段，bearer类型： bearer aa..
    :return: jwt 原始字段
    """
    scheme, param = get_authorization_scheme_param(refresh_token)
    if not refresh_token or scheme.lower() != "bearer":
        raise HTTPException(
            status_code=401,
            detail="Not authenticated, error refresh_token",
            headers={"WWW-Authenticate": "Bearer"},
        )
    return param


def __verify_token2user(db: Session, user_id: Any) -> models.UserToken:
    # 验证用户存在性，并返回user
    user = crud.user.get(db, id=user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found！")
    return user


# ------------- 依赖


def get_current_user(db: Session = Depends(get_db), token: str = Depends(oauth2)) -> models.UserToken:
    """
    检测 http header Authorization 字段的 Bearer 令牌, 并返回当前用户的user
    jwt 默认会验证 令牌中的 exp字段，此token是否已销毁
    """
    try:
        payload = jwt.decode(token, setting.ACCESS_SECRET_KEY)
        token_date = schemas.TokenData(**payload)
    except (jwt.JWTError, ValidationError):
        raise HTTPException(status_code=403, detail='Could not validate credentials')

    return __verify_token2user(db, token_date.subject)


def get_token_refresh(db: Session = Depends(get_db), user_token: str = Depends(oauth2),
                      refresh_token: str = Depends(__util_oauth2, )) -> models.UserToken:
    """
    验证 refresh-token, 及access-token. 返回 user。
    """
    try:
        user_date = schemas.TokenData(
            **jwt.decode(user_token, setting.ACCESS_SECRET_KEY, options={'verify_exp': False}))
    except (jwt.JWTError, ValidationError):
        raise HTTPException(status_code=403, detail='Could not validate credentials')

    try:
        refresh_date = schemas.TokenData(**jwt.decode(refresh_token, setting.ACCESS_SECRET_KEY))
    except (jwt.JWTError, ValidationError):
        raise HTTPException(status_code=482, detail=setting.DEFINED_STATUS['482'])

    if refresh_date.subject != setting.ACCESS_TOKEN_REFRESH_NAME:
        raise HTTPException(status_code=482, detail=setting.DEFINED_STATUS['482'])
    return __verify_token2user(db, user_date.subject)
